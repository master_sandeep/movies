import { combineReducers } from 'redux';
import homeReducer from "../pages/home/redux/reducer"
import searchReducer from "../pages/search/redux/reducer"
import movieDetailReducer from '../pages/movie-details/redux/reducer'
import historyReducer from '../history/reducer'
// Combine all the reducers
const rootReducer = combineReducers({
    home: homeReducer,
    search: searchReducer,
    movieDetail: movieDetailReducer,
    history: historyReducer
})

export default rootReducer;