import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducers from './reduces'; //Import the reducer
import sagas from './sagas';

// Connect our store to the reducers
const sagaMiddleware = createSagaMiddleware();
export default createStore(reducers, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(sagas);
