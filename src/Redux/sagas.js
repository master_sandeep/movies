import { all } from 'redux-saga/effects';
import { watchHomeScreenRequests } from '../pages/home/redux/saga';
import { watchSearchScreenRequests } from '../pages/search/redux/saga';
import { watchMovieDetailsScreenRequests } from '../pages/movie-details/redux/saga'
export default function* sagas() {
  yield all([
    ...watchHomeScreenRequests,
    watchSearchScreenRequests,
    watchMovieDetailsScreenRequests
  ]);
}