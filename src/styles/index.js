import {  Dimensions } from 'react-native';
import { Button, Subheading } from 'react-native-paper';
import styled from 'styled-components/native';
import theme from './theme';
const { width } = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
  justify-content: ${props => (props.centered ? 'center' : 'flex-start')};
  align-items: ${props => (props.centered ? 'center' : 'stretch')};
`;

export const ListFooterContainer = styled.View`
  position: relative;
  width: ${width};
  height: 10%;
  margin-top: 10px;
`;

export const SectionContainer = styled(Container)`
  flex: 4;
  margin-horizontal: 20px;
  padding-bottom: 5px;
`;

export const SectionPageContainer = styled(Container)``;


export const Text = styled.Text``;

export const LoadingText = styled(Subheading)`
  font-weight: bold;
  color: ${theme.colors.primary};
`;

export const Row = styled.View`
  flex-direction: row;
`;

export const CustomButton = styled(Button).attrs(() => ({
  mode: 'outlined',
  uppercase: false
}))`
  border-color: ${props =>
    props.disabled ? theme.colors.lightGray : theme.colors.primary};
  border-width: ${props => (props.outlined ? '2px' : '0px')};
`;