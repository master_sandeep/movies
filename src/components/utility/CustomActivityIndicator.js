
import React from 'react';
import { ActivityIndicator } from 'react-native'
import { ListFooterContainer, SectionPageContainer, LoadingText } from '../../styles'
import theme from '../../styles/theme';

export default CustomActivityIndicator = (props) => {
    if (props.position === 'footer') {
        return (
            <ListFooterContainer>
                <ActivityIndicator animating size="large" color={theme.colors.primary} />
            </ListFooterContainer>
        )
    }
    return (
        <SectionPageContainer centered>
            <ActivityIndicator size="large" color={theme.colors.primary} />
            <LoadingText>Loading</LoadingText>
        </SectionPageContainer>
    )
}