import React from 'react';
import { CardContainer, CardLogo } from './styles';


const MovieCard = ({
  id,
  posterPath,
  loading,
  navigation,
  title
}) => {
  return (
    <CardContainer
      key={id}
      onPress={() => {
        navigation.navigate('MovieDetails', {
          movieID: id,
          movieTitle: title
        });
      }}
      centered
      elevation={3}
      disabled={loading}>
      <CardLogo
        resizeMode="contain"
        source={{ uri: `https://image.tmdb.org/t/p/w200/${posterPath}` }}
      />
    </CardContainer>
  );
};

export default MovieCard;
