import React, { Component } from 'react';
import { Container, Text } from '../styles';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return {
      hasError: true
    };
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      return (
        <Container centered>
          <Text>Oops! Something went wrong!</Text>
        </Container>
      );
    }
    return children;
  }
}

export default ErrorBoundary;
