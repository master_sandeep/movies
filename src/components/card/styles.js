import {Subheading} from 'react-native-paper';
import styled from 'styled-components/native';
import theme from '../../styles/theme';

export const CardShape = styled.TouchableOpacity.attrs(() => ({
  activeOpacity: 1
}))`
  height: 50px;
  padding: 5px;
  border-radius: 6;
  margin: 5px;
  margin-bottom:20;
`;

export const CardContainer = styled(CardShape).attrs(
  (props) => ({
    activeOpacity: 0.7,
    children: props.disabled ? null : props.children
  })
)`
  elevation: ${props => {
    if (props.disabled) {
      return 0;
    }
    return props.elevation ? props.elevation : 2;
  }};
  shadow-color: ${props =>
    props.disabled ? 'transparent' : theme.colors.black};
  shadow-opacity: ${props => (props.disabled ? '0' : '0.3')};
  shadow-radius: ${props => {
    if (props.disabled) {
      return '0';
    }
    return `${props.elevation ? 0.8 * props.elevation : 0.8}`;
  }};
  background-color: ${props => {
    if (props.disabled) {
      return theme.colors.lightGray;
    }
    return props.selected ? theme.colors.primary : '#fff';
  }};
  border-width: ${props => (props.disabled ? '0' : '0.3')};
  border-color: ${props =>
    props.disabled ? 'transparent' : theme.colors.primary};
  justify-content: ${props => (props.centered ? 'center' : 'flex-start')};
  align-items: ${props => (props.centered ? 'center' : 'stretch')};
  min-width: 50px;
`;

export const CardTitle = styled(Subheading)`
  font-weight: bold;
  font-size: 11;
  color: ${props => (props.selected ? '#FFF' : theme.colors.primary)};
`;
