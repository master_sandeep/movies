import React, { Component } from 'react';
import { TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';
import IconRender from '../../components/utility/IconRender'
import Card from '../../components/card';
import theme from '../../styles/theme';
import { homeActionCreators } from './redux/action'
import MovieCard from '../../components/movie-card';
import { CustomActivityIndicator } from '../../components/utility'
import { PageTitle } from '../movie-details/styles'
import { Container } from '../../styles';

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { refreshing: false }
    }
    static navigationOptions = ({ navigation }) => ({
        title: 'Home',
        headerStyle: {
            height: 60,
        },
        headerRight: () => {
            return (
                <TouchableOpacity onPress={() => navigation.navigate('Search')} >
                    <IconRender name='search1' style={{ padding: 10, fontSize: 20, color: theme.colors.primary, margin: 5 }} size={17} familyName='AntDesign' />
                </TouchableOpacity>
            )
        }
    })

    componentDidMount() {
        this.props.fetchGenresRequest();
    }

    handleSelection = (selectedGenreId) => {
        this.props.fetchMoviesByGenresIdRequest(selectedGenreId, 1)
    }

    _handleRefresh = () => {
        this.props.fetchMoviesByGenresIdRequest(this.props.home.selectedGenreId, 1)
    };

    _handleLoadMore = () => {
        if (this.props.home.movies.page !== this.props.home.movies.total_pages) {
            this.props.fetchMoviesByGenresIdRequest(this.props.home.selectedGenreId, this.props.home.movies.page + 1)
        }
    };

    _renderFooter = () => {
        return <CustomActivityIndicator position='footer' />
    };

    render() {
        return (
            <Container centered>
                {!this.props.home.isFetchingGenres &&
                    <FlatList
                        horizontal
                        contentContainerStyle={{
                            height: 70,
                            backgroundColor: theme.colors.lightGray,
                            borderBottomColor: theme.colors.primary,
                            borderWidth: 1
                        }}
                        keyExtractor={item => 'GenresHome' + item.id.toString()}
                        data={this.props.home.genres}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({ item }) =>
                            <Card
                                id={item.id}
                                label={item.name}
                                onPress={() => this.handleSelection(item.id)}
                                selected={item.id === this.props.home.selectedGenreId}
                                disabled={false}
                            />
                        }
                    />

                }
                {(this.props.home.isFetchingGenres || this.props.home.isFetchingMovies) && <CustomActivityIndicator />}
                {this.props.home.movies &&
                    <FlatList
                        contentContainerStyle={{
                            width: '100%',
                            marginHorizontal: 20
                        }}
                        showsHorizontalScrollIndicator={false}
                        numColumns={3}
                        data={this.props.home.movies.results}
                        renderItem={({ item, index }) => (
                            <MovieCard
                                id={item.id}
                                navigation={this.props.navigation}
                                posterPath={item.poster_path}
                                loading={this.props.home.isFetchingMovies}
                                title={item.title}
                            />
                        )}
                        keyExtractor={item => 'movies' + item.id.toString()}
                        ListFooterComponent={this._renderFooter}
                        onRefresh={this._handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={() => this._handleLoadMore()}
                        onEndReachedThreshold={.5}
                        initialNumToRender={10}
                    />
                }
                {(this.props.home.fetchingGenresError || this.props.home.moviesFetchingError) &&
                    <Container>
                        <PageTitle style={{ alignItems: 'center', alignContent: 'center' }}>
                            Failed to load information
                        </PageTitle>
                    </Container>
                }
            </Container>
        );
    }
}



function mapStateToProps(state, props) {
    return {
        home: state.home
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchGenresRequest: () => dispatch(homeActionCreators.fetchGenresRequest()),
        fetchMoviesByGenresIdRequest: (id, page) => dispatch(homeActionCreators.fetchMoviesByGenresIdRequest(id, page)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
