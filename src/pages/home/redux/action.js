
export const CONSTANT = {
    FETCH_GENRES_REQUEST: 'FETCH_GENRES_REQUEST',
    FETCH_GENRES_SUCCESS: 'FETCH_GENRES_SUCCESS',
    FETCH_GENRES_ERROR: 'FETCH_GENRES_ERROR',
    FETCH_MOVIES_BY_GENRE_ID_REQUEST: 'FETCH_MOVIES_BY_GENRE_ID_REQUEST',
    FETCH_MOVIES_BY_GENRE_ID_SUCCESS: 'FETCH_MOVIES_BY_GENRE_ID_SUCCESS',
    FETCH_MOVIES_BY_GENRE_ID_ERROR: 'FETCH_MOVIES_BY_GENRE_ID_ERROR'
}

export const homeActionCreators = {
    fetchGenresRequest() {
        return {
            type: CONSTANT.FETCH_GENRES_REQUEST,
        }
    },
    fetchGenresSuccess(data) {
        return {
            type: CONSTANT.FETCH_GENRES_SUCCESS,
            data
        }
    },
    fetchGenresError(error) {
        return {
            type: CONSTANT.FETCH_GENRES_ERROR,
            error
        }
    },

    fetchMoviesByGenresIdRequest(id, page) {
        return {
            type: CONSTANT.FETCH_MOVIES_BY_GENRE_ID_REQUEST,
            id,
            page
        }
    },

    fetchMoviesByGenresIdSuccess(data) {
        return {
            type: CONSTANT.FETCH_MOVIES_BY_GENRE_ID_SUCCESS,
            data
        }
    },

    fetchMoviesByGenresIdError(error) {
        return {
            type: CONSTANT.FETCH_MOVIES_BY_GENRE_ID_ERROR,
            error
        }
    }
}
