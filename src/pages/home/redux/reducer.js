import { CONSTANT } from './action';

const initialState = {
    isFetchingGenres: false,
    selectedGenreId: null,
    genres: [],
    fetchingGenresError: null,
    isFetchingMovies: false,
    isFetchingMoreMovies: false,
    movies: null,
    moviesFetchingError: null
};

export default homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANT.FETCH_GENRES_REQUEST: {
            return {
                ...initialState,
                isFetchingGenres: true
            }
        }
        case CONSTANT.FETCH_GENRES_SUCCESS: {
            return {
                ...state,
                isFetchingGenres: false,
                selectedGenreId: action.data.genres[0].id,
                genres: action.data.genres
            }
        }
        case CONSTANT.FETCH_GENRES_ERROR: {
            return {
                ...state,
                isFetchingGenres: false,
                fetchingGenresError: action.error
            }
        }
        case CONSTANT.FETCH_MOVIES_BY_GENRE_ID_REQUEST: {
            return {
                ...state,
                isFetchingMovies: action.page === 1,
                isFetchingMoreMovies: action.page !== 1,
                movies: action.page === 1 ? null : state.movies,
                selectedGenreId: action.id
            }
        }
        case CONSTANT.FETCH_MOVIES_BY_GENRE_ID_SUCCESS: {
            return {
                ...state,
                isFetchingMovies: false,
                isFetchingMoreMovies: false,
                movies: state.movies == null ? action.data : mergeResult(state.movies, action.data)
            }
        }
        case CONSTANT.FETCH_MOVIES_BY_GENRE_ID_ERROR: {
            return {
                ...state,
                isFetchingMovies: false,
                isFetchingMoreMovies: false,
                moviesFetchingError: action.error
            }
        }
        default:
            return state;
    }
}

function mergeResult(prev, next) {
    next.results = [...prev.results, ...next.results];
    return next;
}