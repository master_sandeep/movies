import { put, takeLatest, call } from 'redux-saga/effects';
import { CONSTANT, homeActionCreators } from './action';
import { apiCall } from '../../../utility/apiCall';
import { makeHttpRequest } from '../../../utility/HttpsRequestMaker'
import { API_KEY, LANGUAGE_CODE } from '../../../../config';

export function* fetchGenresRequest() {
    let response;
    try {
        const request = yield call(makeHttpRequest, 'GET');
        response = yield call(apiCall, request, `genre/movie/list?api_key=${API_KEY}&language=${LANGUAGE_CODE}`);
        let status = response.status;
        response = yield response.json()
        if (status !== 200) {
            throw new Error(response);
        }
        yield put(homeActionCreators.fetchGenresSuccess(response));
        yield put(homeActionCreators.fetchMoviesByGenresIdRequest(response.genres[0].id, 1));
    } catch (e) {
        yield put(homeActionCreators.fetchGenresError(e.message));
    }
}

export function* fetchMoviesByGenreIdRequest(action) {
    let response;
    try {
        const request = yield call(makeHttpRequest, 'GET');
        response = yield call(apiCall, request, `discover/movie?api_key=${API_KEY}&with_genres=${action.id}&language=${LANGUAGE_CODE}&page=${action.page}`);

        let status = response.status;
        response = yield response.json()
        if (status !== 200) {
            throw new Error(response);
        }
        yield put(homeActionCreators.fetchMoviesByGenresIdSuccess(response));
    } catch (e) {
        yield put(homeActionCreators.fetchMoviesByGenresIdError(e.message));
    }
}


export const watchHomeScreenRequests =
    [
        takeLatest(CONSTANT.FETCH_GENRES_REQUEST, fetchGenresRequest),
        takeLatest(CONSTANT.FETCH_MOVIES_BY_GENRE_ID_REQUEST, fetchMoviesByGenreIdRequest)
    ];
