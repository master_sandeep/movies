
export const CONSTANT = {
    SEARCH_MOVIES_REQUEST: 'SEARCH_MOVIES_REQUEST',
    SEARCH_MOVIES_SUCCESS: 'SEARCH_MOVIES_SUCCESS',
    SEARCH_MOVIES_ERROR: 'SEARCH_MOVIES_ERROR',
    CLEAR_SEARCH_RESULT: 'CLEAR_SEARCH_RESULT',
}

export const searchActionCreators = {
    searchMoviesRequest(text, page) {
        return {
            type: CONSTANT.SEARCH_MOVIES_REQUEST,
            text,
            page
        }
    },
    searchMoviesSuccess(data) {
        return {
            type: CONSTANT.SEARCH_MOVIES_SUCCESS,
            data
        }
    },
    searchMoviesError(error) {
        return {
            type: CONSTANT.SEARCH_MOVIES_ERROR,
            error
        }
    },
    clearSearchResult(){
        return{
            type: CONSTANT.CLEAR_SEARCH_RESULT
        }
    }
}
