import { CONSTANT } from './action';

const initialState = {
    isSearchingMovies: false,
    isSearchingMoveMovies: false,
    movies: null,
    moviesFetchingError: null
};

export default searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANT.SEARCH_MOVIES_REQUEST: {
            return {
                ...initialState,
                isSearchingMovies: action.page === 1,
                isSearchingMoveMovies: action.page !== 1,
                movies: action.page === 1 ? null : state.movies,
            }
        }
        case CONSTANT.SEARCH_MOVIES_SUCCESS: {
            return {
                ...state,
                isSearchingMovies: false,
                isSearchingMoveMovies: false,
                movies: state.movies == null ? action.data : mergeResult(state.movies, action.data)
            }
        }
        case CONSTANT.SEARCH_MOVIES_ERROR: {
            return {
                ...state,
                isSearchingMovies: false,
                isSearchingMoveMovies: false,
                moviesFetchingError: action.error
            }
        }
        case CONSTANT.CLEAR_SEARCH_RESULT: {
            return {
                ...initialState
            }
        }
        default:
            return state;
    }
}

function mergeResult(prev, next) {
    next.results = [...prev.results, ...next.results];
    return next;
}