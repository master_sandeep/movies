import { put, takeLatest, call } from 'redux-saga/effects';
import { CONSTANT, searchActionCreators } from './action';
import { apiCall } from '../../../utility/apiCall';
import { makeHttpRequest } from '../../../utility/HttpsRequestMaker'
import { API_KEY, LANGUAGE_CODE } from '../../../../config';

export function* searchMoviesRequest(action) {
    let response;
    try {
        const request = yield call(makeHttpRequest, 'GET');
        response = yield call(apiCall, request, `search/movie?api_key=${API_KEY}&query=${action.text}&language=${LANGUAGE_CODE}&page=${action.page}`);

        let status = response.status;
        response = yield response.json()
        if (status !== 200) {
            throw new Error(response);
        }
        yield put(searchActionCreators.searchMoviesSuccess(response));
    } catch (e) {
        yield put(searchActionCreators.searchMoviesError(e.message));
    }
}

export const watchSearchScreenRequests = takeLatest(CONSTANT.SEARCH_MOVIES_REQUEST, searchMoviesRequest)

