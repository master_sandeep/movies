import React, { Component } from 'react';
import { FlatList, I18nManager } from 'react-native';
import { connect } from 'react-redux';
import { searchActionCreators } from './redux/action'
import MovieCard from '../../components/movie-card';
import { Container, SectionContainer, SectionPageContainer, LoadingText } from '../../styles'
import { SearchBar } from 'react-native-elements';
import { CustomActivityIndicator } from '../../components/utility'
import theme from '../../styles/theme'

export class Search extends Component {
    constructor(props) {
        super(props);
        this.state = { search: '', }
    }
    static navigationOptions = ({ navigation }) => ({
        header: null
    })

    componentWillMount() {
        this.props.clearSearchResult();
    }

    _handleLoadMore = () => {
        if (this.props.search.movies.page !== this.props.search.movies.total_pages) {
            this.props.searchMoviesRequest(this.state.search, this.props.search.movies.page + 1)
        }
    };

    _renderFooter = () => {
        return <CustomActivityIndicator position='footer' />
    }

    updateSearch = text => {
        this.setState({ search: text });
        if (text.length >= 3) {
            this.props.searchMoviesRequest(text, 1);
        }
    };

    handleBackToHome = () => {
        this.props.navigation.goBack();
    }

    renderSection = (movies) => {
        if (movies.length == 0) {
            return <SectionPageContainer centered>
                <LoadingText>Result not found</LoadingText>
            </SectionPageContainer>
        }
        return (
            <FlatList
                numColumns={3}
                showsHorizontalScrollIndicator={false}
                data={this.props.search.movies.results}
                renderItem={({ item, index }) => (
                    <MovieCard
                        id={item.id}
                        navigation={this.props.navigation}
                        posterPath={item.poster_path}
                        loading={this.props.search.isFetchingMovies}
                        title={item.title}
                    />
                )}
                keyExtractor={item => 'moviesSearch' + item.id.toString()}
                ListFooterComponent={this._renderFooter}
                onEndReached={() => this._handleLoadMore()}
                onEndReachedThreshold={.5}
                initialNumToRender={10}
            />
        )
    }

    render() {
        return (
            <Container>
                <SearchBar
                    inputStyle={{ textAlign: I18nManager.isRTL ? 'right' : 'left' }}
                    showLoading={this.props.search.isSearchingMovies}
                    placeholder="Search"
                    onChangeText={this.updateSearch}
                    value={this.state.search}
                    onClear={() => { this.props.clearSearchResult() }}
                    cancelIcon={{ type: 'material-community', color: theme.colors.lightGray, name: 'share' }}
                    clearIcon={{ size: 30 }}
                    searchIcon={{ size: 25, type: 'font-awesome', color: theme.colors.lightGray, name: I18nManager.isRTL ? 'arrow-right' : 'arrow-left', onPress: this.handleBackToHome }}
                />
                {this.props.search.movies && <SectionContainer>{this.renderSection(this.props.search.movies.results)}</SectionContainer>}
                {this.props.search.moviesFetchingError && <SectionPageContainer centered>
                    <LoadingText>Failed to load information</LoadingText>
                </SectionPageContainer>}
            </Container>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        search: state.search
    }
}

function mapDispatchToProps(dispatch) {
    return {
        searchMoviesRequest: (text, page) => dispatch(searchActionCreators.searchMoviesRequest(text, page)),
        clearSearchResult: () => dispatch(searchActionCreators.clearSearchResult())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
