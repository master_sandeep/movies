import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import {
  FlatList,
} from 'react-navigation';
import { connect } from 'react-redux';
import { updateMoviesVisiterHistory } from '../../history/action'
import { movieDetailActionCreators } from './redux/action';
import theme from '../../styles/theme';
import {
  Badges,
  BadgesRow,
  BadgesShimmer,
  Content,
  ContentBody,
  ContentLabel,
  ContentText,
  DetailsButton,
  InfoContainer,
  Logo,
  LogoContainer,
  LogoShimmer,
  PageContainer,
  PageTitle,
  TextShimmer
} from './styles';


export class MovieDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    getText = (textString, maxLimit) => {
      return ((textString).length > maxLimit) ?
        (((textString).substring(0, maxLimit - 3)) + '...') :
        textString
    }
    return ({
      title: getText(navigation.state.params.movieTitle, 30),
      headerStyle: {
        height: 60,
        marginTop: 0,
        paddingTop: 10,
        paddingBottom: 30,
      },
    })
  }

  componentDidMount() {
    let id = this.props.navigation.getParam('movieID', null);
    let title = this.props.navigation.getParam('movieTitle', null);
    this.props.fetchMovieDetailsRequest(id);
    this.props.updateMoviesVisiterHistory(id, title);
  }

  render() {
    let { isFetchingMovieDetails, movie, FetchingMovieDetailsError } = this.props.movieDetail
    if (isFetchingMovieDetails) {
      return (
        <PageContainer>
          <StatusBar
            barStyle="dark-content"
            backgroundColor={theme.colors.white}
          />
          <LogoContainer>
            <LogoShimmer />
          </LogoContainer>
          <InfoContainer>
            <BadgesRow>
              <BadgesShimmer />
              <BadgesShimmer />
              <BadgesShimmer />
            </BadgesRow>
            <Content loading>
              <ContentBody>
                <TextShimmer widthPercentage="90" />
                <TextShimmer widthPercentage="50" />
                <TextShimmer widthPercentage="75" />
              </ContentBody>
            </Content>
          </InfoContainer>
        </PageContainer>
      );
    }

    if (movie) {
      return (
        <PageContainer>
          <StatusBar
            barStyle="dark-content"
            backgroundColor={theme.colors.white}
          />
          <LogoContainer>
            <Logo
              source={{
                uri: `https://image.tmdb.org/t/p/original/${movie.backdrop_path}`
              }}
            />
          </LogoContainer>
          <InfoContainer>
            <BadgesRow>
              <Badges icon="star-outline" disabled>
                {movie.vote_average}
              </Badges>
              <Badges icon="timelapse" disabled>{`${movie.runtime} min`}</Badges>
              {/* <FavoriteButton movie={movie} /> */}
            </BadgesRow>
            <Content>
              <ContentBody>
                {/* <DetailsButton
                  icon="play-circle-outline"
                  onPress={() =>
                    navigation.navigate(Routes.MOVIE_VIDEOS, {
                      movieID: navigation.getParam('movieID', null),
                      movieTitle: navigation.getParam('movieTitle', null)
                    })
                  }>
                  Ver vídeos
              </DetailsButton> */}
              </ContentBody>
              <ContentBody>
                <ContentLabel>Genres:</ContentLabel>
                <FlatList
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  data={movie.genres}
                  keyExtractor={item => 'cat' + item.id.toString()}
                  renderItem={(item) => (
                    <DetailsButton disabled>{item.item.name}</DetailsButton>
                  )}
                />
              </ContentBody>
              <ContentBody>
                <ContentLabel>Overview:</ContentLabel>
                <ContentText>{movie.overview}</ContentText>
              </ContentBody>
              <ContentBody>
                <ContentLabel>Release date:</ContentLabel>
                <ContentText>{movie.release_date}</ContentText>
              </ContentBody>
            </Content>
          </InfoContainer>
        </PageContainer>
      );
    }
    return (
      <PageContainer centered>
        <StatusBar
          barStyle="light-content"
          backgroundColor={theme.colors.black}
        />
        <PageTitle>
          Failed to load information
        </PageTitle>
      </PageContainer>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    movieDetail: state.movieDetail
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchMovieDetailsRequest: (id) => dispatch(movieDetailActionCreators.fetchMovieDetailsRequest(id)),
    updateMoviesVisiterHistory: (id, title) => dispatch(updateMoviesVisiterHistory(id, title))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieDetails);