import { put, takeLatest, call } from 'redux-saga/effects';
import { CONSTANT, movieDetailActionCreators } from './action';
import { apiCall } from '../../../utility/apiCall';
import { makeHttpRequest } from '../../../utility/HttpsRequestMaker'
import { API_KEY, LANGUAGE_CODE } from '../../../../config';

export function* fetchMovieDetailsRequest(action) {
    let response;
    try {
        const request = yield call(makeHttpRequest, 'GET');
        response = yield call(apiCall, request, `movie/${action.id}?api_key=${API_KEY}&language=${LANGUAGE_CODE}`);
        let status = response.status;
        response = yield response.json()
        if (status !== 200) {
            throw new Error(response);
        }
        yield put(movieDetailActionCreators.fetchMovieDetailsSuccess(response));
    } catch (e) {
        yield put(movieDetailActionCreators.fetchMovieDetailsError(e.message));
    }
}


export const watchMovieDetailsScreenRequests = takeLatest(CONSTANT.FETCH_MOVIE_DETAILS_REQUEST, fetchMovieDetailsRequest);    
