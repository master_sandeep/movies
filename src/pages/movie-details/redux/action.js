
export const CONSTANT = {
    FETCH_MOVIE_DETAILS_REQUEST: 'FETCH_MOVIE_DETAILS_REQUEST',
    FETCH_MOVIE_DETAILS_SUCCESS: 'FETCH_MOVIE_DETAILS_SUCCESS',
    FETCH_MOVIE_DETAILS_ERROR: 'FETCH_MOVIE_DETAILS_ERROR',
}

export const movieDetailActionCreators = {
    fetchMovieDetailsRequest(id) {
        return {
            type: CONSTANT.FETCH_MOVIE_DETAILS_REQUEST,
            id
        }
    },
    fetchMovieDetailsSuccess(data) {
        return {
            type: CONSTANT.FETCH_MOVIE_DETAILS_SUCCESS,
            data
        }
    },
    fetchMovieDetailsError(error) {
        return {
            type: CONSTANT.FETCH_MOVIE_DETAILS_ERROR,
            error
        }
    },
}
