import { CONSTANT } from './action';

const initialState = {
    isFetchingMovieDetails: false,
    movie: null,
    FetchingMovieDetailsError: null
};

export default movieDetailReducer = (state = initialState, action) => {
    switch (action.type) {
        case CONSTANT.FETCH_MOVIE_DETAILS_REQUEST: {
            return {
                ...initialState,
                isFetchingMovieDetails: true
            }
        }
        case CONSTANT.FETCH_MOVIE_DETAILS_SUCCESS: {
            return {
                ...state,
                isFetchingMovieDetails: false,
                movie: action.data
            }
        }
        case CONSTANT.FETCH_MOVIE_DETAILS_ERROR: {
            return {
                ...state,
                isFetchingMovieDetails: false,
                FetchingMovieDetailsError: action.error
            }
        }
        default:
            return state;
    }
}
