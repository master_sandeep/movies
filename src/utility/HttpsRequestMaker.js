//import { getCookies } from './AsyncStorageProvider';

export async function makeHttpRequest(methodName, tokenNeeded, body) {

    request = {
        method: methodName,
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Accept': 'application/json, text/plain, */*'

        },
    };
   
    if (methodName !== 'GET' && body !== undefined) {
        request['body'] = JSON.stringify(body);
    }

    return request;
}