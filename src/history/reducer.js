import { UPDATE_MOVIES_VISITER_HISTORY } from './action';

const initialState = {
    moviesHistory: new Map()
};

export default moviesVisiterHistoryReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_MOVIES_VISITER_HISTORY: {
            return {
                moviesHistory: updateVisiterHistory(state.moviesHistory, action.id, action.title)
            }
        }
        default:
            return state;
    }
}

function updateVisiterHistory(moviesHistory, id, title) {
    if (moviesHistory.has(id)) {
        let previous = moviesHistory.get(id);
        previous.view = previous.view + 1;
        return moviesHistory.set(id, previous)
    }
    return moviesHistory.set(id, { view: 1, title: title });
}
