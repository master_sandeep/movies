export const UPDATE_MOVIES_VISITER_HISTORY = 'UPDATE_MOVIES_VISITER_HISTORY';

export function updateMoviesVisiterHistory(id, title) {
    return {
        type: UPDATE_MOVIES_VISITER_HISTORY,
        id,
        title
    }
}