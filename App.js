import React from 'react'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/pages/home';
import SearchScreen from './src/pages/search';
import MovieDetailScreen from './src/pages/movie-details';
import { Provider as PaperProvider } from 'react-native-paper';
import { theme } from './src/styles/theme';
import { Provider } from 'react-redux';
import store from './src/Redux/store';
import ErrorBoundary from './src/components/error-boundary';

console.disableYellowBox = true;

const AppRoot = createAppContainer(
  createStackNavigator(
    {
      Home: { screen: HomeScreen },
      Search: { screen: SearchScreen },
      MovieDetails: { screen: MovieDetailScreen }
    }
  )
);

const App = () => {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <ErrorBoundary>
          <AppRoot />
        </ErrorBoundary>
      </PaperProvider>
    </Provider>
  );
}

export default App;