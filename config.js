const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = 'aa2f63e2224526c5044131448e2cc2e8';
const LANGUAGE_CODE = 'en-Us'

export {
  API_URL,
  API_KEY,
  LANGUAGE_CODE
};